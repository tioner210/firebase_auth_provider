import 'dart:html';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:google_sign_in/google_sign_in.dart';

final authProvider = StateNotifierProvider<AuthProviderServices, User?>(
    (_) => AuthProviderServices());

class AuthProviderServices extends StateNotifier<User?> {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String? _verificationId;

  AuthProviderServices() : super(null) {
    _init();
  }

  void _init() async {
    try {
      await _googleSignIn.signInSilently();
    } catch (e) {
      print(e);
    }
    _auth.authStateChanges().listen((User? event) {
      state = event;
    });
    return;
  }

  Future<User?> logInWithPassword(
      {required String email, required String password}) async {
    try {
      final credenciales = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      return credenciales.user;
    } catch (e) {
      return Future.error('Error Revisa tus credenciales');
    }
  }

  Future logInWithGoogle() async {
    var googleSignInAccount = await _googleSignIn.signIn();

    var googleSignInAuthentication = await googleSignInAccount?.authentication;

    AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication?.accessToken,
      idToken: googleSignInAuthentication?.idToken,
    );
    await _auth.signInWithCredential(credential);
  }

  Future singnInWithNumber(String number) async {
    await _auth.verifyPhoneNumber(
        phoneNumber: number,
        codeAutoRetrievalTimeout: (String verificationId) {
          _verificationId = verificationId;
        },
        codeSent: (String verificationId, int? forceResendingToken) {
          _verificationId = verificationId;
        },
        verificationCompleted: (PhoneAuthCredential phoneAuthCredential) async {
          await _auth.signInWithCredential(phoneAuthCredential);
        },
        verificationFailed: (FirebaseAuthException error) {});
  }

  Future confirma(String code) async {
    var credential = PhoneAuthProvider.credential(
        verificationId: _verificationId ?? '', smsCode: code);

    await _auth.signInWithCredential(credential);
  }

  Future singOut() async {
    await _auth.signOut();
    await _googleSignIn.signOut();
    await FirebaseAuth.instance.signOut();
  }
}
