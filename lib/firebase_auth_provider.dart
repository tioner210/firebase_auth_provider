/// Support for doing something awesome.
///
/// More dartdocs go here.
library firebase_auth_provider;

export 'src/firebase_auth_provider_base.dart';

import 'package:firebase_auth/firebase_auth.dart';
// TODO: Export any libraries intended for clients of this package.
